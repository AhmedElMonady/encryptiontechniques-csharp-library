﻿using EncryptionTechniques.Misc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace EncryptionTechniques.AdvancedEncryptionStandard
{
    public partial class AES
    {
        public static string[,] SBox(string[,] bytes, string[,] mode)
        {
            for(int i = 0; i < bytes.GetLength(0); i++)
            {
                for(int j = 0; j < bytes.GetLength(1); j++)
                {
                    var Byte = bytes[i, j];
                    var str = Tools.HexToBinary(Byte);
                    var r = Tools.BinaryToInt(str.Substring(0, str.Length / 2));
                    var c = Tools.BinaryToInt(str.Substring(str.Length / 2, str.Length / 2));
                    bytes[i, j] = mode[r, c];
                }
            }
            return bytes;
        }
        static string[] GetColumn(string[,] matrix, int columnNumber)
        {
            return Enumerable.Range(0, matrix.GetLength(0))
                    .Select(x => matrix[x, columnNumber])
                    .ToArray();
        }
        static string[] GetRow(string[,] matrix, int rowNumber)
        {
            return Enumerable.Range(0, matrix.GetLength(1))
                    .Select(x => matrix[rowNumber, x])
                    .ToArray();
        }
        public static string[,] RowShifting(string[,] array)
        {
            string[,] shifted = new string[array.GetLength(0), array.GetLength(1)];

            for(int i = 0; i < shifted.GetLength(0); i++)
            {
                for(int j = 0; j < shifted.GetLength(1); j++)
                {
                    var ShiftOffset = j-i < 0 ? j-i+4 : j-i; 
                    shifted[i, j] = array[i, 3 - ShiftOffset];
                }
            }

            array = shifted;
            
            for(int i = 0; i < array.GetLength(1); i++)
            {
                var row = GetRow(shifted, i);
                Array.Reverse(row);
                for(int j = 0; j < array.GetLength(1); j++)
                {
                    shifted[i, j] = row[j];
                }
            }
            
            return shifted;
        }
        public static string[,] MixColumns(string[,] matrix, string[,] mode)
        {
            int[,] intResult = new int[matrix.GetLength(0), matrix.GetLength(1)];

            string[,] Result = new string[matrix.GetLength(0), matrix.GetLength(1)];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j = 0; j < matrix.GetLength(1); j++)
                {
                    intResult[i, j] = 0;
                    for(int k = 0; k < matrix.GetLength(1); k++)
                    {
                        intResult[i, j] += Tools.HexToInt(matrix[i, k]) * Tools.HexToInt(mode[k, j]);
                    }
                    Result[i, j] = Tools.IntToHex(intResult[i, j]);
                }
            }

            return Result;
        }
        public static string[,] AddRoundKey(string[,] Matrix, string[,] RoundKey)
        {
            for(int i = 0; i < Matrix.GetLength(0); i++)
            {
                for(int j = 0; j < Matrix.GetLength(1); j++)
                {
                    Matrix[i, j] = Tools.BinaryToHex(Tools.XOR(Tools.HexToBinary(Matrix[i, j]), Tools.HexToBinary(RoundKey[i, j])));
                }
            }
            return Matrix;
        }
        public static string[] RotWordAndSub(string[] word, string[,] mode)
        {
            string[] Rotated = new string[word.Length];

            for(int i = 0;i<word.Length - 1; i++)
            {
                Rotated[i] = word[i + 1];
            }

            Rotated[word.Length - 1] = word[0];

            for(int i = 0; i < Rotated.Length; i++)
            {
                var Byte = Rotated[i];
                var str = Tools.HexToBinary(Byte);
                var r = Tools.BinaryToInt(str.Substring(0, str.Length / 2));
                var c = Tools.BinaryToInt(str.Substring(str.Length / 2, str.Length / 2));
                Rotated[i] = mode[r, c];
            }

            return Rotated;
        }
        public static string [,] StringToMatrix(string key)
        {
            string[,] matrix = new string[4, 4];
            for(int i = 0;i<4; i++)
            {
                for(int j = 0; j < 4; j++)
                {

                }
            }
            return matrix;
        }
        public static string MatrixToString(string[,] key)
        {
            string matrix = "";
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {

                }
            }
            return matrix;
        }
        public static List<string[,]> KeyExpansion(string[,] Cipherkey)
        {
            return new List<string[,]>();
        }
        public static string Encrypt(string plaintext, string key)
        {
            String[,] state = new string[4, 4];
            String[,] RoundKey = new string[4, 4];
            string[,] Cipherkey = new string[4, 4];
            var ciphertext = "";
            //convert key string to matrix string
            var Keys = KeyExpansion(Cipherkey);
            state = AddRoundKey(state, RoundKey);

            for (int i = 0; i < 9; i++)
            {
                state = SBox(state, EncryptionSBox);
                state = RowShifting(state);
                state = MixColumns(state, EncryptionMixColumns);
                state = AddRoundKey(state, Keys[i]);
            }


            state = SBox(state, EncryptionSBox);
            state = RowShifting(state);
            state = AddRoundKey(state, Keys[9]);
            //convert ciphertext matrix to string
            return ciphertext;
        }
        public static string Decrypt()
        {
            String[,] state = new string[4, 4];
            String[,] RoundKey = new string[4, 4];
            string[,] Cipherkey = new string[4, 4];
            var plaintext = "";
            //convert key string to matrix string
            var Keys = KeyExpansion(Cipherkey);
            state = AddRoundKey(state, RoundKey);

            for (int i = 0; i < 9; i++)
            {
                state = SBox(state, DecryptionSBox);
                state = RowShifting(state);
                state = MixColumns(state, DecryptionMixColumn);
                state = AddRoundKey(state, Keys[i]);
            }


            state = SBox(state, DecryptionSBox);
            state = RowShifting(state);
            state = AddRoundKey(state, Keys[9]);
            //convert plaintext matrix to string
            return plaintext;
        }
    }
}
