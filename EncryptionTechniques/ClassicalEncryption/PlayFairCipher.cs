﻿using EncryptionTechniques.Misc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EncryptionTechniques.ClassicalEncryption
{
    public static class PlayFairCipher
    {
        public static char[,] FillMatrix(List<char> key, List<char> alpha)
        {
            char[,] Matrix = new char[5, 5];
            int key_count = 0;

            foreach (var c in alpha)
                key.Add(c);
            for(int i =0 ; i < 5; i++)
            {
                for(int j = 0; j < 5; j++)
                {
                    Matrix[i, j] = key[key_count++];
                }
            }
            return Matrix;
        }
        public static List<char> InitializeAlpha()
        {
            List<char> alpha = new List<char>();
            for (int i = 0; i < 26; i++)
            {
                alpha.Add((char)(i + 97));
            }

            return alpha;
        }
        public static char[,] GenerateMatrix(string key, string plaintext)
        {
            List<char> uniqueKey = Tools.RemoveDuplicates(Tools.RemoveSpaces(key.ToLower()));
            List<char> Alpha = InitializeAlpha();

            for (int i = 0; i < uniqueKey.Count; i++)
                Alpha.Remove(uniqueKey[i]);

            if (plaintext.Contains('i'))
                Alpha.Remove('j');
            else if (plaintext.Contains('j'))
                Alpha.Remove('i');
            else 
                Alpha.Remove('j');

            return FillMatrix(uniqueKey, Alpha);
        }
        public static String CorrectDuplicates(string text)
        {
            List<char> Text = text.ToList<char>();
            var str = "";
            for(int i = 0; i < Text.Count-1; i++)
            {
                if (Text[i] == Text[i + 1] && Text[i] != 'x')
                    Text.Insert(i + 1, 'x');
            }
            foreach (var st in Text)
                str += st;

            return str;
        }
        //No Consideration if last letter is duplicate x or odd sized ending with x
        public static string FixLength(string text)
        {
            if (text.Length % 2 != 0)
                return text += "x";
            return text;
        }
        public static string TaskProcess(char item1, char item2, char[,] matrix, Process process)
        {
            var block = "";
            var index1 = Tools.GetIndex(matrix, item1);
            var index2 = Tools.GetIndex(matrix, item2);
            char c1 = '.' , c2 = '.';
            
            if(index1 != null && index2 != null)
            {
                if (process == Process.Encrypt)
                {
                    //same row
                    if (index1.i == index2.i)
                    {
                        //if next char is at the end of the array, loop back to the begining
                        //else next char is within bounds
                        if (index1.j == matrix.GetLength(1) - 1)
                            c1 = Tools.GetChar(matrix, new Position
                            {
                                i = index1.i,
                                j = 0
                            });
                        else //
                            c1 = Tools.GetChar(matrix, new Position
                            {
                                i = index1.i,
                                j = index1.j + 1
                            });

                        if (index2.j == matrix.GetLength(1) - 1)
                            c2 = Tools.GetChar(matrix, new Position
                            {
                                i = index2.i,
                                j = 0
                            });
                        else 
                            c2 = Tools.GetChar(matrix, new Position
                            {
                                i = index2.i,
                                j = index2.j + 1
                            });
                    }
                    //same column
                    else if(index1.j == index2.j)
                    {
                        if (index1.i == matrix.GetLength(0) - 1)
                            c1 = Tools.GetChar(matrix, new Position
                            {
                                i = 0,
                                j = index1.j
                            });
                        else
                            c1 = Tools.GetChar(matrix, new Position
                            {
                                i = index1.i + 1,
                                j = index1.j
                            }) ;

                        if (index2.i == matrix.GetLength(0) - 1)
                            c2 = Tools.GetChar(matrix, new Position
                            {
                                i = 0,
                                j = index2.j
                            });
                        else
                            c2 = Tools.GetChar(matrix, new Position
                            {
                                i = index2.i + 1,
                                j = index2.j
                            });
                    }
                    //rectangular form
                    else
                    {
                        c1 = Tools.GetChar(matrix, new Position
                        {
                            i = index1.i,
                            j = index2.j
                        });
                        c2 = Tools.GetChar(matrix, new Position
                        {
                            i = index2.i,
                            j = index1.j
                        });
                    }
                }
                else if (process == Process.Decrypt)
                {
                    //same row
                    if(index1.i == index2.i)
                    {
                        if (index1.j == 0)
                            c1 = Tools.GetChar(matrix, new Position
                            {
                                i = index1.i,
                                j = matrix.GetLength(1) - 1
                            });
                        else
                            c1 = Tools.GetChar(matrix, new Position
                            {
                                i = index1.i,
                                j = index1.j - 1
                            });

                        if (index2.j == 0)
                            c2 = Tools.GetChar(matrix, new Position
                            {
                                i = index2.i,
                                j = matrix.GetLength(1) - 1
                            });
                        else
                            c2 = Tools.GetChar(matrix, new Position
                            {
                                i = index2.i,
                                j = index2.j - 1
                            });
                    }
                    //same column
                    else if(index1.j == index2.j)
                    {
                        if (index1.i == 0)
                            c1 = Tools.GetChar(matrix, new Position
                            {
                                i = matrix.GetLength(0) - 1,
                                j = index1.j
                            });
                        else
                            c1 = Tools.GetChar(matrix, new Position
                            {
                                i = index1.i - 1,
                                j = index1.j
                            });
                        if (index2.i == 0)
                            c2 = Tools.GetChar(matrix, new Position
                            {
                                i = matrix.GetLength(0) - 1,
                                j = index2.j
                            });
                        else
                            c2 = Tools.GetChar(matrix, new Position
                            {
                                i = index2.i - 1,
                                j = index2.j
                            });
                    }
                    //rectangular form
                    else
                    {
                        c1 = Tools.GetChar(matrix, new Position
                        {
                            i = index1.i,
                            j = index2.j
                        });
                        c2 = Tools.GetChar(matrix, new Position
                        {
                            i = index2.i,
                            j = index1.j
                        });
                    }
                }
            }
            block += c1;
            block += c2;
            return block;
        }
        public static string Encrypt(string key, string plaintext)
        {
            var Ciphertext = "";
            var Plaintext = FixLength(CorrectDuplicates(Tools.RemoveSpaces(plaintext.ToLower())));
            var EncryptionMatrix = GenerateMatrix(key,Plaintext);
            for(int i = 0; i < Plaintext.Length-1; i+=2)
            {
                Ciphertext += TaskProcess(Plaintext[i], Plaintext[i + 1], EncryptionMatrix,Process.Encrypt);
            }
            return Ciphertext;
        }
        public static string Decrypt(string key, string ciphertext)
        {
            var Plaintext = "";
            //not necessary but preferable - better safe than sorry
            var Ciphertext = FixLength(CorrectDuplicates(Tools.RemoveSpaces(ciphertext.ToLower())));
            var DecryptionMatrix = GenerateMatrix(key,Ciphertext);
            for(int i = 0; i < Ciphertext.Length-1; i+=2)
            {
                Plaintext += TaskProcess(Ciphertext[i], Ciphertext[i + 1], DecryptionMatrix, Process.Decrypt);
            }
            return Plaintext;
        }
    }
}
