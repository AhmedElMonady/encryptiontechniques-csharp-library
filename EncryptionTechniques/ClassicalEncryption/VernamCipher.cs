﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EncryptionTechniques.ClassicalEncryption
{
    public class VernamCipher
    {
        public static string Encrypt(string plaintext, string key)
        {
            var ciphertext = "";
            plaintext = plaintext.ToUpper();
            key = key.ToUpper();
            var randomizer = plaintext.Length;

            for(int i = 0; i < plaintext.Length; i++)
            {
                ciphertext += Convert.ToChar((((plaintext[i] - 'A') ^ (key[i] -'A')) % 26) + 'A');
            }

            return ciphertext;
        }
    }
}
