﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace EncryptionTechniques.ClassicalEncryption
{
    public static class HillCipher
    {
        static string CleanPlaintext(string plaintext, int keyMatrixSize)
        {
            while (plaintext.Length % keyMatrixSize != 0)
            {
                plaintext += 'x';
            }

            return plaintext.ToUpper();
        }

        static int[,] GenerateKeyMatrix(List<int> key, int keyMatrixSize)
        {
            int[,] keyMatrix = new int[keyMatrixSize, keyMatrixSize];

            if (key.Count < (keyMatrixSize * keyMatrixSize))
            {
                var diff = (keyMatrixSize * keyMatrixSize) - key.Count;
                for (int i = 0; i < diff; i++)
                {
                    key.Add(0);
                }
            }

            for (int i = 0; i < keyMatrixSize; i++)
            {
                for (int j = 0; j < keyMatrixSize; j++)
                {
                    keyMatrix[i, j] = key[i * keyMatrixSize + j];
                }
            }

            return keyMatrix;
        }

        public static string Encrypt(string plaintext, int keyMatrixSize, List<int> key)
        {
            plaintext = CleanPlaintext(plaintext, keyMatrixSize);
            var keyMatrix = GenerateKeyMatrix(key, keyMatrixSize);
            var ciphertext = "";
            //to avoid the less than multiples of 3 strings lengths.
            if (plaintext.Length % 3 != 0) plaintext += 'x';

            for (int k = 0; k < plaintext.Length; k += 3)
            {
                var plaintextSubstring = plaintext.Substring(k, 3);
                for (int i = 0; i < keyMatrixSize; i++)
                {
                    var char_code = 0;
                    for (int j = 0; j < keyMatrixSize; j++)
                    {
                        char_code += keyMatrix[i, j] * (plaintextSubstring[j] - 'A');

                    }
                    ciphertext += Convert.ToChar((char_code % 26) + 'A');
                }
            }

            return ciphertext;
        }
    }
}
