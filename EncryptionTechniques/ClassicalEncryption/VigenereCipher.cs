﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EncryptionTechniques.ClassicalEncryption
{
    public class VigenereCipher
    {
        static string GenerateKey(string key, string plaintext, bool mode)
        {
            string GeneratedKey = "";
            int KeyLength = key.Length;
            int PlaintextLength = plaintext.Length;

            int loops = (PlaintextLength / KeyLength) + 1;

            if(mode == true) //auto mode
            {
                GeneratedKey = key + plaintext;
            }
            else if(mode == false) //repeating mode
            {
                for(int i = 0; i < loops; i++)
                {
                    GeneratedKey += key;
                }
            }

            if (GeneratedKey.Length == PlaintextLength) return GeneratedKey;
            else if (GeneratedKey.Length > PlaintextLength) return GeneratedKey.Substring(0, PlaintextLength);

            return GeneratedKey;
        }
        public static string Encrypt(string plaintext, string key, bool mode)
        {
            string ciphertext = "";
           
            var GeneratedKey = GenerateKey(key, plaintext, mode);

            GeneratedKey = GeneratedKey.ToUpper();
            plaintext = plaintext.ToUpper();

            for(int i = 0; i < GeneratedKey.Length; i++)
            {
                ciphertext += Convert.ToChar((((GeneratedKey[i]) + (plaintext[i])) % 26) + 'A');
            }

            return ciphertext;
        }
        public static string Decrypt(string ciphertext, string key, bool mode)
        {
            string plaintext = "";

            var GeneratedKey = GenerateKey(key, ciphertext, mode);

            GeneratedKey = GeneratedKey.ToUpper();
            ciphertext = ciphertext.ToUpper();

            for (int i = 0; i < GeneratedKey.Length; i++)
            {
                plaintext += Convert.ToChar((((ciphertext[i]) - (GeneratedKey[i]) + 26) % 26) + 'A');
            }

            return plaintext;
        }
    }
}
