﻿using System;

namespace EncryptionTechniques.ClassicalEncryption
{
    public static class CaesarCipher
    {
        public static string Encrypt(string plaintext, int key)
        {
            var ciphertext = "";
            var len = plaintext.Length;
            char offset;

            for (int i = 0; i < len; i++)
            {
                if (Char.IsWhiteSpace(plaintext[i]))
                    continue;
                offset = Char.IsUpper(plaintext[i]) ? 'A' : 'a';
                ciphertext += Convert.ToChar(((plaintext[i] + key - offset) % 26) + offset);
            }

            return ciphertext;
        }
        public static string Decrypt(string ciphertext, int key)
        {
            return Encrypt(ciphertext, 26 - key);
        }
    }
}
