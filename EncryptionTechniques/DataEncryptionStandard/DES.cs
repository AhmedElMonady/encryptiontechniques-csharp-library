﻿using EncryptionTechniques.Misc;
using System;
using System.Collections.Generic;

namespace EncryptionTechniques.DataEncryptionStandard
{
    public partial class DES
    {
        static string PBox(string key, List<int> PC)
        {
            string KeyPlus = "";
            for (int i = 0; i < PC.Count; i++)
            {
                KeyPlus += key[PC[i] - 1];
            }
            return KeyPlus;
        }
        static string SBox(int box, string block)
        {
            string RowIndex = block[0] + "" + block[block.Length - 1];

            var ColumnIndex = block.Substring(1, 4);

            return Tools.IntToBinary(SBox_Tables[box, Tools.BinaryToInt(RowIndex), Tools.BinaryToInt(ColumnIndex)], 4);
        }
        static List<string> KeysGenerator(string key)
        {
            var KeyPlus = PBox(key, PermutedChoice_1);
            var C0 = KeyPlus.Substring(0, KeyPlus.Length / 2);
            var D0 = KeyPlus.Substring(KeyPlus.Length / 2, KeyPlus.Length / 2);

            List<string> Cx = new List<string>();
            List<string> Dx = new List<string>();

            var Cn = C0;
            var Dn = D0;

            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < LeftCircularShiftTable[i]; j++)
                {
                    Cn = Tools.LeftCircularShift(Cn);
                    Dn = Tools.LeftCircularShift(Dn);
                }

                Cx.Add(Cn);
                Dx.Add(Dn);
            }

            List<string> Kn = new List<string>();
            for (int i = 0; i < 16; i++)
            {
                Kn.Add(PBox(Cx[i] + Dx[i], PermutedChoice_2));
            }

            return Kn;

        }
        static string F(string R, string Key)
        {
            var ExpandedR = PBox(R, ExpansionPermutation);
            var XORResult = Tools.XOR(ExpandedR, Key);
            var SubstitutedString = "";
            for (int i = 0; i < 8; i++)
            {
                var BlockExtract = XORResult.Substring(i * 6, 6);
                SubstitutedString += SBox(i, BlockExtract);
            }
            SubstitutedString = PBox(SubstitutedString, PermutationTable);
            return SubstitutedString;
        }
        static string DESFunction(string Key, string Plaintext, Process Method)
        {
            var key = Tools.HexToBinary(Key);
            var plaintext = Tools.HexToBinary(Plaintext);
            var GeneratedKeys = KeysGenerator(key);

            var PlaintextIP = PBox(plaintext, InitialPermutation);

            string L0 = PlaintextIP.Substring(0, PlaintextIP.Length / 2);
            string R0 = PlaintextIP.Substring(PlaintextIP.Length / 2, PlaintextIP.Length / 2);
            string LPrev = L0;
            string RPrev = R0;
            string LCurrent, RCurrent;

            if (Method == Process.Decrypt) GeneratedKeys.Reverse();

            for (int i = 0; i < 16; i++)
            {
                LCurrent = RPrev;
                RCurrent = Tools.XOR(LPrev, F(RPrev, GeneratedKeys[i]));

                RPrev = RCurrent;
                LPrev = LCurrent;
            }

            var FinalRound = RPrev + LPrev;

            FinalRound = PBox(FinalRound, InversePermutation);

            var Ciphertext = Tools.BinaryToHex(FinalRound);

            return Ciphertext.ToUpper();
        }
        public static string Encrypt(String Key, String Plaintext, int Rounds)
        {
            var Ciphertext = Plaintext;
            for(int i = 0; i < Rounds; i++)
            {
                Ciphertext = DESFunction(Key, Ciphertext, Process.Encrypt);
            }
            return Ciphertext;
        }
        public static string Decrypt(String Key, String Ciphertext, int Rounds)
        {
            var Plaintext = Ciphertext;
            for(int i = 0; i < Rounds; i++)
            {
                Plaintext = DESFunction(Key, Plaintext, Process.Decrypt);
            }
            return Plaintext;
        }
    }
}
