﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EncryptionTechniques.Misc
{
    public enum Process
    {
        Encrypt = 0,
        Decrypt = 1
    }
    public class Position
    {
        public int i { get; set; }
        public int j { get; set; }
        public Position(int i, int j)
        {
            this.i = i;
            this.j = j;
        }
        public Position()
        {
                
        }
    }
    
    public static class Tools
    {
        public static Position GetIndex(char[,] matrix, char c)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == c) return new Position(i, j);
                }
            }
            return null;
        }
        public static char GetChar(char[,] matrix, Position p)
        {
            if (p.i < matrix.GetLength(0) && p.j < matrix.GetLength(1))
                return matrix[p.i, p.j];
            return '\0';
        }
        public static string RemoveSpaces(string text)
        {
            string noSpace = "";
            for (int i = 0; i < text.Length; i++)
            {
                if (Char.IsWhiteSpace(text[i]))
                    continue;
                noSpace += text[i];
            }
            return noSpace;
        }
        public static List<char> RemoveDuplicates(string text)
        {
            return text.Distinct().ToList<char>();
        }
        public static string HexToBinary(string HexString)
        {
            return Convert.ToString(Convert.ToInt64(HexString, 16), 2).PadLeft(HexString.Length * 4, '0');
        }
        public static string IntToBinary(int Number, int BitNum)
        {
            return Convert.ToString(Number, 2).PadLeft(BitNum, '0');
        }
        public static string BinaryToHex(string BinaryString)
        {
            return Convert.ToInt64(BinaryString, 2).ToString("X");
        }
        public static int BinaryToInt(string BinaryString)
        {
            return Convert.ToInt32(BinaryString, 2);
        }
        public static string IntToHex(int num)
        {
            return num.ToString("X");
        }
        public static int HexToInt(string hex)
        {
            return Convert.ToInt32(hex, 16);
        }
        public static string LeftCircularShift(string key)
        {
            var ShiftedKey = "";

            var temp = key[0];
            ShiftedKey += key.Substring(1, key.Length - 1) + temp;

            return ShiftedKey;
        }
        public static string XOR(string operandA, string operandB)
        {
            string xor = "";

            for (int i = 0; i < operandA.Length; i++)
            {
                xor += Convert.ToString(Convert.ToInt32(operandA[i]) ^ Convert.ToInt32(operandB[i]));
            }

            return xor;
        }
    }
}
