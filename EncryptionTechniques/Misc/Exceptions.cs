﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EncryptionTechniques.Misc
{
    public class OutOfBoundException : Exception
    {
        public OutOfBoundException() : base("Invalid Round Number, Rounds Must be between 1 and 16")
        {

        }
    }
}
